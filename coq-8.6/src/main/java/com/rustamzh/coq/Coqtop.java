package com.rustamzh.coq;

import com.rustamzh.coq.commands.*;
import com.rustamzh.coq.feedback.Error;
import com.rustamzh.coq.feedback.Feedback;
import com.rustamzh.coq.feedback.commandsFeedback.AddedReturn;
import com.rustamzh.coq.feedback.commandsFeedback.EvarsReturn;
import com.rustamzh.coq.feedback.commandsFeedback.GoalReturn;
import com.rustamzh.coq.feedback.commandsFeedback.QuitReturn;
import com.rustamzh.coq.process.CoqProcess;
import com.rustamzh.coq.server.ReadServer;
import com.rustamzh.coq.server.WriteServer;
import com.rustamzh.coq.utilites.ErrorException;
import com.rustamzh.coq.utilites.OptionPair;
import com.rustamzh.coq.utilites.Success;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Stream;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Coqtop {

    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    public WriteServer writeServer;

    @Autowired
    private ReadServer readServer;

    private BlockingQueue<Feedback> blockingQueue;
    private BlockingQueue<Feedback> statusQueue;

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private CoqProcess coqProcess;


    @PostConstruct
    public void init() {
        BlockingQueue<Feedback> blockingQueue = (BlockingQueue<Feedback>) beanFactory.getBean("getBlockingQueue");
        this.setBlockingQueue(blockingQueue);
        readServer.getDispatcher().setBlockingQueue(blockingQueue);
        BlockingQueue<Feedback> statusQueue = (BlockingQueue<Feedback>) beanFactory.getBean("getBlockingQueue");
        this.setStatusQueue(statusQueue);
        readServer.getDispatcher().setStatusQueue(statusQueue);
        this.start();
    }

    public Stream<Feedback> getStatusStream(){
        return this.statusQueue.stream();
    }

    private void start() {
        LOG.debug("Coq started");
        coqProcess.start(this.readServer.getPort(),this.writeServer.getPort());
        readServer.start();
        writeServer.start();
        new Thread(readServer).start();
    }

    private Feedback command(Command com) {
        this.writeServer.write(com.toString());
        Feedback feedback = null;
        try {
            feedback = blockingQueue.take();
            if (!blockingQueue.isEmpty()) {
                feedback = blockingQueue.take();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (feedback instanceof Success && com instanceof Goals)
            return new GoalReturn(null, null, null, null);
        else if (feedback instanceof Success && com instanceof Evars)
            return new EvarsReturn(Optional.empty());
        else if (feedback instanceof Success && com instanceof Quit)
            return new QuitReturn();
        return feedback;
    }

    private Feedback run(Command com) {

        if (com instanceof Add) {
            Add add = (Add) com;
            String command = add.getCommand();
            command = StringUtils.trim(command);
            command = StringUtils.appendIfMissing(command, ".");
            int nCommands = StringUtils.countMatches(command, ".");
            if (nCommands > 1 && !(command.contains("Require") || command.contains("Import") || command.contains("LoadPath"))) {
                String[] strings = StringUtils.split(command, ".");
                int state = add.getStateId();
                for (int i = 0; i < nCommands - 1; i++) {
                    AddedReturn addedReturn = (AddedReturn) this.command(new Add(strings[i] + ".", state, add.isVerbose()));
                    state = addedReturn.getNextStateId();
                }
                return this.command(new Add(strings[nCommands - 1] + ".", state, add.isVerbose()));
            } else
                return this.command(com);
        } else
            return this.command(com);
    }

    public Feedback execute(Command com) throws ErrorException {
        Feedback obj = run(com);
        if (!obj.getClass().equals(Error.class)) {
            return obj;
        } else {
            throw new ErrorException(Collections.singletonList((Error) obj));
        }
    }

    public boolean setDefaults() {
        List<OptionPair> list = new LinkedList<>();
        list.add(new OptionPair<Optional<Integer>>(Arrays.asList("Printing", "Width"), "intvalue", Optional.of(60)));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Coercions"), "boolvalue", false));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Matching"), "boolvalue", true));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Notations"), "boolvalue", true));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Existential", "Instances"), "boolvalue", false));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Implicit"), "boolvalue", false));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "All"), "boolvalue", false));
        list.add(new OptionPair<Boolean>(Arrays.asList("Printing", "Universes"), "boolvalue", false));
        this.command(new SetOptions(list));
        return true;
    }

    public void compileVintoVO(java.io.File Vfile) throws Exception{
        coqProcess.compileVintoVO(Vfile);
    }

    public void shutdown() {
        try {
            QuitReturn quitReturn = (QuitReturn) execute(new Quit());
        } catch (ErrorException e) {
            e.printStackTrace();
        }
        coqProcess.stop();
    }

    public WriteServer getWriteServer() {
        return writeServer;
    }

    public void setWriteServer(WriteServer writeServer) {
        this.writeServer = writeServer;
    }

    public ReadServer getReadServer() {
        return readServer;
    }

    public void setReadServer(ReadServer readServer) {
        this.readServer = readServer;
    }

    public BlockingQueue<Feedback> getBlockingQueue() {
        return blockingQueue;
    }

    public void setBlockingQueue(BlockingQueue<Feedback> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    public BlockingQueue<Feedback> getStatusQueue() {
        return statusQueue;
    }

    public void setStatusQueue(BlockingQueue<Feedback> statusQueue) {
        this.statusQueue = statusQueue;
    }

    public CoqProcess getCoqProcess() {
        return coqProcess;
    }

    public void setCoqProcess(CoqProcess coqProcess) {
        this.coqProcess = coqProcess;
    }
}
