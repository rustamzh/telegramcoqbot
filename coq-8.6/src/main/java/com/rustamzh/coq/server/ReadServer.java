package com.rustamzh.coq.server;

import com.rustamzh.coq.utilites.AnswerDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Scanner;
import java.util.regex.Pattern;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ReadServer extends AbstarctServer implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final Pattern MATCH = Pattern.compile("<value [\\s\\S]*?<\\/value>|<feedback [\\s\\S]*?<\\/feedback>|<message[\\s\\S]*?<\\/message>");
    private Scanner sc;

    @Autowired
    private AnswerDispatcher dispatcher;

    public ReadServer(int port) {
        super(port);
    }

    public ReadServer() {
        super();
    }

    @Override
    public void run() {
        try {
            sc = new Scanner(connectionSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String token = sc.findWithinHorizon(MATCH, 0);
        while (token != null) {
            dispatcher.dispatch(token);
            token = sc.findWithinHorizon(MATCH, 0);
        }
        LOG.info("Coq exited");
    }

    public Scanner getSc() {
        return sc;
    }

    public AnswerDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(AnswerDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }
}

