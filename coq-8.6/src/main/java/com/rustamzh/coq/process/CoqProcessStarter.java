package com.rustamzh.coq.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.*;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CoqProcessStarter implements CoqProcess {

    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ExecutorService executor;
    private volatile Process coq;

    @Override
    public void start(int readerPort, int writerPort) {
        executor = Executors.newSingleThreadExecutor();
        executor.submit(()->{
            try {
                StringBuilder error = new StringBuilder();
                StringBuilder input = new StringBuilder();


                LOG.info("Starting process - com.rustamzh.coq");

                //+" -control-channel 127.0.0.1:23001:23002"
                coq = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "coqtop -toploop coqidetop -main-channel 127.0.0.1:" + readerPort + ":" + writerPort});
                LOG.debug("Running shell command /bin/sh -c coqtop -toploop coqidetop -main-channel 127.0.0.1:" + readerPort + ":" + writerPort);
                StreamGobbler errorGobbler = new
                        StreamGobbler(coq.getErrorStream(), "ERROR");

                StreamGobbler inputGobbler = new
                        StreamGobbler(coq.getInputStream(), "INPUT");
                ExecutorService executorService = Executors.newSingleThreadExecutor();

                executorService.submit(errorGobbler);
                executorService.submit(inputGobbler);
                int exitCode = coq.waitFor();
                executorService.shutdown();
                LOG.debug(input.toString());
                LOG.debug(error.toString());
                LOG.debug("Coq returned " + exitCode);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void stop() {
        if (coq != null && executor != null){
            coq.destroy();
            executor.shutdown();
        } else {
            LOG.info(coq==null?"Stop: Coq is null":"Stop: Coq is not null");
            LOG.info(executor==null?"Stop: Executor is null": "Stop: Executor is not null");
        }
    }

    public void compileVintoVO(java.io.File Vfile) throws Exception {

        String filename = Vfile.getAbsolutePath();

        if (!filename.endsWith(".v")) {
             throw new Exception("Wrong file extension: expected .v");
        }

        Callable<String> compiler = () -> {
            StringBuilder error = new StringBuilder();
            try {
                LOG.info("Starting process - coqc");

                Process coqc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "coqc " + filename});
                StreamGobbler errorGobbler = new
                        StreamGobbler(coqc.getErrorStream(), "ERROR");

                StreamGobbler inputGobbler = new
                        StreamGobbler(coqc.getInputStream(), "INPUT");

                ExecutorService executorService = Executors.newSingleThreadExecutor();

                Future<String> future = executorService.submit(errorGobbler);
                executorService.submit(inputGobbler);

                int exitCode = coqc.waitFor();
                LOG.info("Coqc returned " + exitCode);

                error.append(future.get());
                executorService.shutdown();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
            return error.toString();
        };

        FutureTask<String> futureTask = new FutureTask<String>(compiler);
        Thread thread1 = new Thread(futureTask);
        thread1.start();
        try {
            String output = futureTask.get();
            LOG.info(output);
            if (!Objects.equals(output, "")) {
                LOG.info(Objects.equals(output, "")+"");
                throw new Exception(output);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    private class StreamGobbler implements Callable<String>
    {
        InputStream is;
        String type;

        StreamGobbler(InputStream is, String type)
        {
            this.is = is;
            this.type = type;
        }

        @Override
        public String call() {
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ( (line = br.readLine()) != null)
                    stringBuilder.append(type).append(">").append(line);

            } catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
            return stringBuilder.toString();
        }
    }

}
