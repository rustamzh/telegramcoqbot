package com.rustamzh.coq.commands;

import com.rustamzh.coq.utilites.OptionPair;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class SetOptions extends Command {
    List<OptionPair> list;

    public SetOptions(List<OptionPair> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        String BeginCommand = "<call val=\"SetOptions\"><list>";
        String BodyCommandBegin = "<pair><list>";
        String BodyCommandEnd = "</option_value></pair>";
        String EndCommand = "</list></call>";
        for (OptionPair op : list) {
            List<String> optionNameList = op.getOptionName();
            for (String optionName : optionNameList) {
                BodyCommandBegin += "<string>" + optionName + "</string>";
            }
            BodyCommandBegin += "</list>";
            BodyCommandBegin += "<option_value val=\"" + op.getOptionType() + "\">";
            if (optionNameList.get(0) != null && optionNameList.get(0).equals("Printing") &&
                    optionNameList.get(1) != null && optionNameList.get(1).equals("Depth")) {
                String str = (String) op.getValue();
                BodyCommandBegin += "<int>" + str + "</ints>";
            } else if (Objects.equals(op.getOptionType(), "intvalue")) {
                Optional<Integer> optional = (Optional<Integer>) op.getValue();
                if (optional.isPresent()) {
                    BodyCommandBegin += "<option val=\"some\">" + "<int>" + optional.get() + "</int>" + "</option>";
                } else {
                    BodyCommandBegin += "<option val=\"none\"/>";
                }
            } else if (Objects.equals(op.getOptionType(), "boolvalue")) {
                Boolean bool = (Boolean) op.getValue();
                BodyCommandBegin += "<bool val=\"" + bool.toString() + "\"/>";
            } else if (Objects.equals(op.getOptionType(), "stringvalue")) {
                String str = (String) op.getValue();
                BodyCommandBegin += "<string>" + str + "</string>";
            } else if (Objects.equals(op.getOptionType(), "stringoptvalue")) {
                Optional<String> optional = (Optional<String>) op.getValue();
                if (optional.isPresent()) {
                    BodyCommandBegin += "<option val=\"some\">" + "<string>" + optional.get() + "</string>" + "</option>";
                } else {
                    BodyCommandBegin += "<option val=\"none\"/>";
                }
            }
            BeginCommand += "</option_value>";
        }
        return BeginCommand + BodyCommandBegin + BodyCommandEnd + EndCommand;
    }
}
