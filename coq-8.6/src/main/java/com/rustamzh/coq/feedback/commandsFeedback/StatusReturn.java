package com.rustamzh.coq.feedback.commandsFeedback;

import com.rustamzh.coq.feedback.Feedback;

import java.util.List;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class StatusReturn extends Feedback {

    List<String> status_path;
    String proofName;
    List<String> pendingProof;
    int id_current_proof;

    public StatusReturn() {
        super(0);
    }

    public StatusReturn(List<String> status_path, String proofName, List<String> pendingProof, int id_current_proof) {
        super(0);
        this.status_path = status_path;
        this.proofName = proofName;
        this.pendingProof = pendingProof;
        this.id_current_proof = id_current_proof;
    }

    public StatusReturn(List<String> status_path, List<String> pendingProof, int id_current_proof) {
        super(0);
        this.status_path = status_path;
        this.proofName = "None";
        this.pendingProof = pendingProof;
        this.id_current_proof = id_current_proof;
    }

    public List<String> getStatus_path() {
        return status_path;
    }

    public String getProofName() {
        return proofName;
    }

    public List<String> getPendingProof() {
        return pendingProof;
    }

    public int getId_current_proof() {
        return id_current_proof;
    }

    @Override
    public String toString() {
        return "StatusReturn{" +
                "status_path=" + status_path +
                ", proofName='" + proofName + '\'' +
                ", pendingProof=" + pendingProof +
                ", id_current_proof=" + id_current_proof +
                '}';
    }
}
