package com.rustamzh.coq.feedback.commandsFeedback;

import com.rustamzh.coq.feedback.Feedback;
import com.rustamzh.coq.utilites.Goal;
import com.rustamzh.coq.utilites.Pair;

import java.util.List;
import java.util.Optional;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class GoalReturn extends Feedback {

    Optional<List<Goal>> currentGoals;
    Optional<List<Pair<List<Goal>, List<Goal>>>> bgGoals;
    Optional<List<Goal>> shelvedGoals;
    Optional<List<Goal>> abandonedGoals;

    public GoalReturn(List<Goal> currentGoals, List<Pair<List<Goal>, List<Goal>>> bgGoals, List<Goal> shelvedGoals, List<Goal> abandonedGoals) {
        super(0);
        this.currentGoals = currentGoals != null ? Optional.of(currentGoals) : Optional.empty();
        this.bgGoals = bgGoals != null ? Optional.of(bgGoals) : Optional.empty();
        this.shelvedGoals = shelvedGoals != null ? Optional.of(shelvedGoals) : Optional.empty();
        this.abandonedGoals = abandonedGoals != null ? Optional.of(abandonedGoals) : Optional.empty();
    }

    public Optional<List<Goal>> getCurrentGoals() {
        return currentGoals;
    }

    public Optional<List<Pair<List<Goal>, List<Goal>>>> getBgGoals() {
        return bgGoals;
    }

    public Optional<List<Goal>> getShelvedGoals() {
        return shelvedGoals;
    }

    public Optional<List<Goal>> getAbandonedGoals() {
        return abandonedGoals;
    }

    @Override
    public String toString() {
        return "GoalReturn{" +
                "currentGoals=" + currentGoals +
                ", bgGoals=" + bgGoals +
                ", shelvedGoals=" + shelvedGoals +
                ", abandonedGoals=" + abandonedGoals +
                '}';
    }
}
