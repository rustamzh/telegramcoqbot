package com.rustamzh.coq.feedback;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class Message extends Feedback {
    private String level;
    private int start;
    private int stop;
    private String message;

    public Message(int stateId, String level, int start, int stop, String message) {
        super(stateId);
        this.level = level;
        this.start = start;
        this.stop = stop;
        this.message = message;
    }

    public Message(String level, String message) {
        super(0);
        this.level = level;
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public int getStart() {
        return start;
    }

    public int getStop() {
        return stop;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "level='" + level + '\'' +
                ", start=" + start +
                ", stop=" + stop +
                ", message='" + message + '\'' +
                '}';
    }
}
