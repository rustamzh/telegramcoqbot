package com.rustamzh.coq.utilites;

import java.util.List;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class OptionPair<T> {
    private List<String> optionName;
    private boolean sync;
    private boolean deprecated;
    private String shortname;
    private String optionType;
    private T value;

    public OptionPair(List<String> optionName, boolean sync, boolean deprecated, String description, String optionType, T value) {
        this.optionName = optionName;
        this.sync = sync;
        this.deprecated = deprecated;
        this.shortname = description;
        this.optionType = optionType;
        this.value = value;
    }

    public OptionPair(List<String> optionName, String optionType, T value) {
        this.optionName = optionName;
        this.optionType = optionType;
        this.value = value;
    }

    @Override
    public String toString() {
        return "OptionPair{" +
                "optionName=" + getOptionName() +
                ", sync=" + isSync() +
                ", deprecated=" + isDeprecated() +
                ", description='" + getDescription() + '\'' +
                ", optionType='" + getOptionType() + '\'' +
                ", value='" + getValue() + '\'' +
                '}';
    }

    public List<String> getOptionName() {
        return optionName;
    }

    public boolean isSync() {
        return sync;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    public String getDescription() {
        return shortname;
    }

    public String getOptionType() {
        return optionType;
    }

    public T getValue() {
        return value;
    }
}
