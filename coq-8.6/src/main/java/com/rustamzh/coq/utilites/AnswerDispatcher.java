package com.rustamzh.coq.utilites;

import com.rustamzh.coq.feedback.*;
import com.rustamzh.coq.feedback.Error;
import com.rustamzh.coq.feedback.commandsFeedback.*;
import com.rustamzh.coq.utilites.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AnswerDispatcher {
    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private BlockingQueue<Feedback> blockingQueue;

    private BlockingQueue<Feedback> statusQueue;

    public void dispatch(String reading) {
        LOG.debug("{}", reading);

        try {

            String xmlDoc = "<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp \"&#160;\">]>";
            Document answer = DocumentHelper.parseText(xmlDoc + reading);
            LOG.debug("{}", answer.asXML());
            if (answer.getRootElement().element("coq_info") != null) {
                Iterator<Element> it = answer.getRootElement().element("coq_info").elementIterator();
                blockingQueue.put(new Coq_info(it.next().getStringValue(),
                        it.next().getStringValue(),
                        it.next().getStringValue(),
                        it.next().getStringValue()));
            } else if (answer.getRootElement().element("state_id") != null && answer.getRootElement().elements().size() == 1) {
                blockingQueue.put(new InitReturn(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val"))));
            } else if (answer.getRootElement().element("pair") != null &&
                    answer.getRootElement().element("pair").element("state_id") != null) {
                Element el = answer.getRootElement().element("pair");
                int newStateId = Integer.parseInt(el.element("state_id").attributeValue("val"));
                String message = el.element("pair").element("string").getStringValue();
                if (el.element("pair").element("union").attributeValue("val").equals("in_l"))
                    blockingQueue.put(new AddedReturn(newStateId, message));
                else {
                    int stateid = Integer.parseInt(el.element("union").element("state_id").attributeValue("val"));
                    blockingQueue.put(new AddedReturn(newStateId, stateid, message));
                }
            } else if (answer.getRootElement().element("union") != null) {
                if (answer.getRootElement().element("union").attributeValue("val").equals("in_l"))
                    blockingQueue.put(new EditAtReturn());
                else {
                    Element pair = answer.getRootElement().element("union").element("pair");
                    Iterator<Element> iterator = pair.element("pair").elementIterator("state_id");
                    blockingQueue.put(new EditAtReturn(Integer.parseInt(pair.element("state_id").attributeValue("val")),
                            Integer.parseInt(iterator.next().attributeValue("val")),
                            Integer.parseInt(iterator.next().attributeValue("val"))));
                }
            } else if (answer.getRootElement().element("status") != null) {
                Element el = answer.getRootElement().element("status");
                List<Element> list = el.elements("list");
                List<Element> listElPath = list.remove(0).elements("string");
                List<String> listPath = listElPath.stream().map(Element::getStringValue).collect(Collectors.toList());
                List<Element> listElProof = list.remove(0).elements("string");
                List<String> listProof = listElProof.stream().map(Element::getStringValue).collect(Collectors.toList());
                int proofNumber = Integer.parseInt(el.element("int").getStringValue());
                if (el.element("option").attributeValue("val").equals("some")) {
                    String proofName = el.element("option").element("string").getStringValue();
                    blockingQueue.put(new StatusReturn(listPath, proofName, listProof, proofNumber));
                } else
                    blockingQueue.put(new StatusReturn(listPath, listProof, proofNumber));

            } else if (answer.getRootElement().getName().equals("value") && answer.getRootElement().attributeValue("val").equals("fail")) {
                if (answer.getRootElement().attribute("loc_s") != null) {
                    blockingQueue.put(new Error(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val")),
                            answer.getRootElement().element("richpp").getStringValue(),
                            Integer.parseInt(answer.getRootElement().attributeValue("loc_s")),
                            Integer.parseInt(answer.getRootElement().attributeValue("loc_e"))));
                } else
                    blockingQueue.put(new Error(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val")),
                            answer.getRootElement().element("richpp").asXML()));
            } else if (answer.getRootElement().element("list") != null &&
                    answer.getRootElement().element("list").element("pair") != null) {
                GetOptionsReturn getOptionsReturn = getOptionsResult(answer);
                blockingQueue.put(getOptionsReturn);
            } else if (answer.getRootElement().getName().equals("value") && answer.getRootElement().attributeValue("val").equals("good") &&
                    answer.getRootElement().element("option") != null &&
                    answer.getRootElement().element("option").attributeValue("val").equals("some") &&
                    answer.getRootElement().element("option").element("goals") != null) {
                GoalReturn goalReturn = getGoalReturn(answer);
                blockingQueue.put(goalReturn);
            } else if (answer.getRootElement().element("option") != null &&
                    answer.getRootElement().element("option").element("list") != null &&
                    answer.getRootElement().element("option").element("list").element("evar") != null) {
                List<Element> el = answer.getRootElement().element("option").element("list").elements("evar");
                List<String> evarsList = el.stream().map(Element::getStringValue).collect(Collectors.toList());
                blockingQueue.put(new EvarsReturn(Optional.of(evarsList)));
            } else if (answer.getRootElement().getName().equals("message")) {
                String level = answer.getRootElement().element("message_level").attributeValue("val");
                String message = answer.getRootElement().element("richpp").element("_").getStringValue();
                if (answer.getRootElement().element("option").attributeValue("val").equals("none")) {
                    LOG.info("{}", String.valueOf(new Message(level, message)));
                    if (level.equals("notice")) {
                        blockingQueue.put(new QueryReturn(answer.getRootElement().element("richpp").getStringValue()));
                    }
                } else
                    LOG.error("ATTENTION: MESSAGE CASE TO AUGMENT");
            } else if (answer.getRootElement().element("option") != null &&
                    (answer.getRootElement().element("option").element("list") != null ||
                            answer.getRootElement().element("option").attributeValue("val").equals("none"))) {
                blockingQueue.put(new Success());
            } else if (answer.getRootElement().element("feedback_content") != null &&
                    answer.getRootElement().element("feedback_content").attributeCount() > 0 &&
                    answer.getRootElement().element("feedback_content").attribute("val").getValue().equals("processed")) {
                statusQueue.put(new Processed(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val"))));
                LOG.debug("{}", new Processed(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val"))));
            } else if (answer.getRootElement().element("feedback_content") != null &&
                    answer.getRootElement().element("feedback_content").attributeCount() > 0 &&
                    answer.getRootElement().element("feedback_content").attribute("val").getValue().equals("addedaxiom")) {
                statusQueue.put(new AddedAxiom(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val"))));
                LOG.debug("{}", new AddedAxiom(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val"))));
            } else if (answer.getRootElement().element("feedback_content") != null &&
                    answer.getRootElement().element("feedback_content").attribute("val").getValue().equals("processingin")) {
                statusQueue.put(new Processing(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val")),
                        answer.getRootElement().element("feedback_content").element("string").getStringValue()));
                LOG.debug("{}", new Processing(Integer.parseInt(answer.getRootElement().element("state_id").attributeValue("val")),
                        answer.getRootElement().element("feedback_content").element("string").getStringValue()));
            }
        } catch (DocumentException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private GoalReturn getGoalReturn(Document answer) {
        List<Element> goalsList = answer.getRootElement().element("option").element("goals").elements("list");
        List<Goal> currentGoal = new LinkedList<>();
        List<Goal> abandonedGoals = new LinkedList<>();
        List<Goal> shelvedGoals = new LinkedList<>();
        List<Pair<List<Goal>, List<Goal>>> backgroundGoals = new LinkedList<>();
        List<List> goalsLists = new LinkedList<>();
        goalsLists.add(currentGoal);
        goalsLists.add(backgroundGoals);
        goalsLists.add(shelvedGoals);
        goalsLists.add(abandonedGoals);
        int i = 0;
        for (Element list : goalsList) {

            if (list.element("goal") != null) {
                List<Element> goals = list.elements("goal");

                for (Element goal : goals) {
                    String goalId = goal.element("string").getStringValue();
                    String goalString = goal.element("richpp").getStringValue();
                    List<String> hypListString = new LinkedList<>();
                    if (goal.element("list").element("richpp") != null) {
                        List<Element> hypList = goal.element("list").elements("richpp");
                        for (Element hyp : hypList) {
                            hypListString.add(hyp.getStringValue());
                        }
                    }
                    if (hypListString.size() != 0)
                        goalsLists.get(i).add(new Goal(goalId, goalString, Optional.of(hypListString)));
                    else
                        goalsLists.get(i).add(new Goal(goalId, goalString, Optional.empty()));
                }
            } else if ((list.element("pair") != null)) {
                List<Element> pairs = list.elements("pair");

                for (Element pair : pairs) {
                    Element list1 = (Element) pair.elements("list").get(0);
                    Element list2 = (Element) pair.elements("list").get(1);
                    List<Element> list1goals = list1.elements("goal");
                    List<Element> list2goals = list2.elements("goal");
                    List<Goal> goalList1 = new LinkedList<>();
                    List<Goal> goalList2 = new LinkedList<>();
                    List<Pair> listLists = new LinkedList<>();
                    listLists.add(new Pair(goalList1, list1goals));
                    listLists.add(new Pair(goalList2, list2goals));
                    for (Pair<List<Goal>, List<Element>> l : listLists) {
                        for (Element goal : l.getValue()) {
                            String goalId = goal.element("string").getStringValue();
                            String goalString = goal.element("richpp").getStringValue();
                            List<String> hypListString = new LinkedList<>();
                            if (goal.element("list").element("richpp") != null) {
                                List<Element> hypList = goal.element("list").elements("richpp");
                                for (Element hyp : hypList) {
                                    hypListString.add(hyp.getStringValue());
                                }
                            }
                            if (hypListString.size() != 0)
                                l.key.add(new Goal(goalId, goalString, Optional.of(hypListString)));
                            else
                                l.key.add(new Goal(goalId, goalString, Optional.empty()));
                        }
                    }
                    backgroundGoals.add(new Pair(goalList1, goalList2));
                }
            }
            i++;
        }
        return new GoalReturn(currentGoal.size() != 0 ? currentGoal : null,
                backgroundGoals.size() != 0 ? backgroundGoals : null,
                shelvedGoals.size() != 0 ? shelvedGoals : null,
                abandonedGoals.size() != 0 ? abandonedGoals : null);
    }

    private GetOptionsReturn getOptionsResult(Document answer) {
        List<Element> optionPairs = answer.getRootElement().element("list").elements("pair");
        List<OptionPair> listOptionPair = new LinkedList<>();
        for (Element el : optionPairs) {
            List<Element> optionList = el.element("list").elements("string");
            List<String> optionName = optionList.stream().map(Element::getStringValue).collect(Collectors.toList());
            Element option_state = el.element("option_state");
            List<Element> booleans = option_state.elements("bool");
            boolean sync = Boolean.parseBoolean(booleans.get(0).attributeValue("val"));
            boolean deprecated = Boolean.parseBoolean(booleans.get(1).attributeValue("val"));
            String description = option_state.element("string").getStringValue();
            Element option = option_state.element("option_value");
            String option_type = option.attributeValue("val");
            switch (option_type) {
                case "stringvalue":
                    String value = option.element("string").getStringValue();
                    listOptionPair.add(new OptionPair<String>(optionName, sync, deprecated, description, "stringvalue", value));
                    break;
                case "stringoptvalue":
                    if (option.element("option").attributeValue("val").equals("none"))
                        listOptionPair.add(new OptionPair<Optional<String>>(optionName, sync, deprecated, description, "stringoptvalue", Optional.empty()));
                    else {
                        String value1 = option.element("option").element("string").getStringValue();
                        listOptionPair.add(new OptionPair<Optional<String>>(optionName, sync, deprecated, description, "stringoptvalue", Optional.of(value1)));
                    }
                    break;
                case "intvalue":
                    if (option.element("option").attributeValue("val").equals("none"))
                        listOptionPair.add(new OptionPair<Optional<Integer>>(optionName, sync, deprecated, description, "intvalue", Optional.empty()));
                    else {
                        try {
                            Integer value1 = Integer.parseInt(option.element("option").element("int").getStringValue());
                            listOptionPair.add(new OptionPair<Optional<Integer>>(optionName, sync, deprecated, description, "intvalue", Optional.of(value1)));
                        } catch (NumberFormatException e) {
                            BigInteger value1 = new BigInteger(option.element("option").element("int").getStringValue());
                            listOptionPair.add(new OptionPair<Optional<BigInteger>>(optionName, sync, deprecated, description, "intvalue", Optional.of(value1)));

                        }
                    }
                    break;
                case "boolvalue":
                    Boolean value2 = Boolean.parseBoolean(option.element("bool").attributeValue("val"));
                    listOptionPair.add(new OptionPair<Boolean>(optionName, sync, deprecated, description, "boolvalue", value2));
                    break;
                default:
                    LOG.error("PROTOCOL ERROR: GETSTRING");
            }
        }
        return new GetOptionsReturn(listOptionPair);
    }

    public BlockingQueue<Feedback> getBlockingQueue() {
        return blockingQueue;
    }

    public void setBlockingQueue(BlockingQueue<Feedback> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    public BlockingQueue<Feedback> getStatusQueue() {
        return statusQueue;
    }

    public void setStatusQueue(BlockingQueue<Feedback> statusQueue) {
        this.statusQueue = statusQueue;
    }
}
