package com.rustamzh.coq.utilites;

import com.rustamzh.coq.feedback.Feedback;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public class Coq_info extends Feedback {
    String coqtop_version;
    String protocol_version;
    String release_date;
    String compile_date;

    public Coq_info(String version, String date, String date1, String date2) {
        super(0);
        this.coqtop_version = version;
        this.protocol_version = date;
        this.release_date = date1;
        this.compile_date = date2;
    }

    public Coq_info() {
        super(0);

    }

    @Override
    public String toString() {
        return "Coq_info{" +
                "coqtop_version='" + coqtop_version + '\'' +
                ", protocol_version='" + protocol_version + '\'' +
                ", release_date='" + release_date + '\'' +
                ", compile_date='" + compile_date + '\'' +
                '}';
    }
}
