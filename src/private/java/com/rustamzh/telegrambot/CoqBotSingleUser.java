package com.rustamzh.telegrambot;

import com.harium.dotenv.Env;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Update;

import java.util.List;


/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
public class CoqBotSingleUser extends CoqBot{

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        for(Update update:updates){
            if (update.hasMessage() && update.getMessage().getFrom().getUserName() != null && update.getMessage().getFrom().getUserName().equals(getUserName())){
                this.onUpdateReceived(update);
            } else {
                LOG.info("Someone unauthorized is trying to access this bot");
            }
        }
    }

    public String getBotUsername() {

        return Env.get("TelegramBotName");
    }

    public String getBotToken() {

        return Env.get("TelegramBotToken");
    }

    private String getUserName(){
        return Env.get("TelegramUsername");
    }

}
