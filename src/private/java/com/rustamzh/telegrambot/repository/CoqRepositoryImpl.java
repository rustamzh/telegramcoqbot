package com.rustamzh.telegrambot.repository;


import com.rustamzh.coq.Coqtop;
import com.rustamzh.coq.utilites.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Repository
public class CoqRepositoryImpl implements CoqRepository {

    @Autowired
    private ConcurrentHashMap<Long, Pair<Coqtop, Integer>> current_users;

    @Autowired
    ApplicationContext applicationContext;

    public Pair<Coqtop, Integer> getCoq(Long userId){
        return current_users.computeIfAbsent(userId, k ->
        {
            Coqtop coqtop = applicationContext.getBean(Coqtop.class);
            return new Pair<>(coqtop, 0);
        });
    }

    public boolean updateState(Long userId, int stateId){
        if (current_users.containsKey(userId)){
            Coqtop coqtop = current_users.get(userId).key;
            current_users.put(userId,new Pair<>(coqtop,stateId));
            return true;
        }
        else
            return false;
    }

    @Override
    public boolean deleteCoq(Long userId) {
        return current_users.computeIfPresent(userId, (k,v) -> {
            v.key.shutdown();
            return null;
        }) == null ;
    }

    @Override
    public boolean isPresent(Long userId) {
        return current_users.containsKey(userId);
    }


    public ConcurrentHashMap<Long, Pair<Coqtop, Integer>> getCurrent_users() {
        return current_users;
    }

    public void setCurrent_users(ConcurrentHashMap<Long, Pair<Coqtop, Integer>> current_users) {
        current_users = current_users;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
