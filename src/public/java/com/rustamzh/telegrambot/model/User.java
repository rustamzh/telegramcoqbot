package com.rustamzh.telegrambot.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Document
public class User {


    @Id @Indexed private long userId;
    private long lastCommandTime;
    private long lastStateId;

    @DBRef
    private List<Command> history;

    public User() {
    }

    public User(long userId, long lastCommandTime, long lastStateId) {
        this.userId = userId;
        this.lastCommandTime = lastCommandTime;
        this.lastStateId = lastStateId;
    }

    public User(long userId, long lastCommandTime, long lastStateId, List<Command> history) {
        this.userId = userId;
        this.lastCommandTime = lastCommandTime;
        this.lastStateId = lastStateId;
        this.history = history;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getLastCommandTime() {
        return lastCommandTime;
    }

    public void setLastCommandTime(long lastCommandTime) {
        this.lastCommandTime = lastCommandTime;
    }

    public List<Command> getHistory() {
        return history;
    }

    public void setHistory(List<Command> history) {
        this.history = history;
    }

    public long getLastStateId() {
        return lastStateId;
    }

    public void setLastStateId(long lastStateId) {
        this.lastStateId = lastStateId;
    }
}
