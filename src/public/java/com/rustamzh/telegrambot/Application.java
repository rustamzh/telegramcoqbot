package com.rustamzh.telegrambot;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.harium.dotenv.Env;
import com.rustamzh.coq.utilites.AppConfig;
import com.rustamzh.telegrambot.repository.CommandRepository;
import com.rustamzh.telegrambot.repository.UserRepository;
import metrics_influxdb.HttpInfluxdbProtocol;
import metrics_influxdb.InfluxdbReporter;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.InfluxDBIOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.concurrent.TimeUnit;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@SpringBootApplication
@Import(AppConfig.class)
@PropertySource("classpath:strings.properties")
@PropertySource("classpath:build.properties")
@EnableMongoRepositories(basePackageClasses = {UserRepository.class, CommandRepository.class})
@EnableScheduling
public class Application{


    private String InfluxdbHost = Env.get("InfluxdbHost");
    private int InfluxdbPort = Integer.parseInt(Env.get("InfluxdbPort"));
    private String InfluxdbDatabase = Env.get("InfluxdbDatabase");

    @Autowired
    private CoqBotMultiUser coqBot;


    @Autowired
    private MetricRegistry registry;

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(Application.class, args);
    }


    @Bean
    CommandLineRunner runner (){
        return args -> {
            if (isConnected()) {
                report();
            }
            startBot();
        };
    }

    private void report(){
        final ScheduledReporter reporter = InfluxdbReporter.forRegistry(registry)
                .protocol(new HttpInfluxdbProtocol(InfluxdbHost, InfluxdbPort, InfluxdbDatabase))
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .skipIdleMetrics(true)
                .build();
        reporter.start(1, TimeUnit.MINUTES);
    }

    private void startBot(){
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(coqBot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private boolean isConnected() {
        InfluxDB influxDB = null;
        boolean exit = true;
        try {
        influxDB = InfluxDBFactory.connect("http://"+InfluxdbHost+":"+InfluxdbPort, "root", "root");

        if (!influxDB.databaseExists(InfluxdbDatabase)) {
            influxDB.createDatabase(InfluxdbDatabase);
        }
        } catch (InfluxDBIOException e) {
            exit = false;
        } finally {
            if (influxDB != null){
                influxDB.close();
            }
        }
        return exit;

    }

}
