package com.rustamzh.telegrambot.repository;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.rustamzh.coq.Coqtop;
import com.rustamzh.coq.commands.Add;
import com.rustamzh.coq.commands.Init;
import com.rustamzh.coq.feedback.commandsFeedback.AddedReturn;
import com.rustamzh.coq.feedback.commandsFeedback.InitReturn;
import com.rustamzh.coq.utilites.ErrorException;
import com.rustamzh.coq.utilites.Pair;
import com.rustamzh.telegrambot.model.Command;
import com.rustamzh.telegrambot.model.User;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Repository
@Primary
public class CoqMongoRepository implements CoqRepository{

    @Autowired private UserRepository userRepository;
    @Autowired private CommandRepository commandRepository;
    @Autowired private ConcurrentHashMap<Long, ImmutableTriple<Coqtop, Integer, Long>> onlineUsers;
    @Autowired ApplicationContext applicationContext;

    @Autowired
    private MetricRegistry metricRegistry;


    private final int timeout = 15;
    private final TimeUnit timeoutUnit = TimeUnit.MINUTES;

    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @PostConstruct
    public void init(){
        metricRegistry.register("coqbot.online", (Gauge<Integer>) () -> onlineUsers.size());
    }

    @Override
    public Pair<Coqtop, Integer> getCoq(Long userId) {
        LOG.info("GetCoq");
        if (onlineUsers.containsKey(userId)){
            Triple<Coqtop, Integer, Long> triple = onlineUsers.get(userId);
            return new Pair<>(triple.getLeft(), triple.getMiddle());
        } else if (userRepository.existsByUserId(userId)){
            return getRestoredCoqtop(userId);
        } else {
            Coqtop coqtop = getCoqtop(userId);
            userRepository.save(new User(userId, System.currentTimeMillis(), 0));
            return new Pair<>(coqtop, 0);
        }
    }

    private Coqtop getCoqtop(Long userId) {
        Coqtop coqtop = applicationContext.getBean(Coqtop.class);
        onlineUsers.put(userId, new ImmutableTriple<>(coqtop, 0, System.currentTimeMillis()));
        return coqtop;
    }

    @Override
    public boolean updateState(Long userId, int stateId) {
        if (onlineUsers.containsKey(userId)) {
            Coqtop coqtop = onlineUsers.get(userId).left;
            onlineUsers.put(userId, new ImmutableTriple<>(coqtop, stateId, System.currentTimeMillis()));
            return true;
        }
        return false;
    }

    private Pair<Coqtop, Integer> getRestoredCoqtop(Long userId) {
        if (getSavedStateId(userId) == 0){
            return new Pair<>(getCoqtop(userId), 0);
        } else {
            Pair<Coqtop, Integer> pair = restoreCoqtop(getHistory(userId));
            assert pair!= null;
            onlineUsers.put(userId, new ImmutableTriple<>(pair.key, pair.value, System.currentTimeMillis()));
            return new Pair<>(pair.key, pair.value);
        }
    }

    @Override
    public boolean updateState(Long userId, String command, int stateId) {
        if (command == null) {
            return false;
        } else {
            updateState(userId,stateId);
            saveCommand(userId,command,stateId);
            return true;
        }
    }

    @Override
    public boolean deleteCoq(Long userId) {
        ImmutableTriple<Coqtop, Integer, Long> triple= onlineUsers.get(userId);
        triple.getLeft().shutdown();
        onlineUsers.remove(userId);
        return false;
    }

    @Override
    public boolean isPresent(Long userId) {
        return onlineUsers.containsKey(userId) || userRepository.existsByUserId(userId);
    }

    @Scheduled(fixedDelay = 900000)
    public void scheduledClean(){
        LOG.info("Runnable...");

        for (Iterator<ImmutableTriple<Coqtop, Integer, Long>> iterator = onlineUsers.values().iterator(); iterator.hasNext();){
            ImmutableTriple<Coqtop, Integer, Long> item = iterator.next();
            if (System.currentTimeMillis() - item.getRight() > timeoutUnit.toMillis(timeout)) {
                LOG.info("Coq is deleted");
                item.getLeft().shutdown();
                iterator.remove();
            }
        }
    }

    private boolean saveCommand(Long userId, String command, int stateId){
        User user = userRepository.findOneByUserId(userId);
        List<Command> commands = user.getHistory();
        user.setLastStateId(stateId);
        if (commands != null) {
            Command command1 = new Command(command, stateId);
            commandRepository.save(command1);
            commands.add(command1);
            userRepository.save(user);
        } else {
            Command command1 = new Command(command, stateId);
            commandRepository.save(command1);
            user.setHistory(Collections.singletonList(command1));
            userRepository.save(user);
        }
        return true;
    }

    private long getSavedStateId(Long userId){
        User user = userRepository.findOneByUserId(userId);
        return user.getLastStateId();
    }

    private Pair<Coqtop, Integer> restoreCoqtop(List<Command> commands) {
        Coqtop coqtop = applicationContext.getBean(Coqtop.class);
        InitReturn init = null;
        try {
            init = (InitReturn) coqtop.execute(new Init());
        } catch (ErrorException e) {
            LOG.error("Error during loading coqtop: Init throws exception.");
            return null;
        }
        AddedReturn addedReturn;
        int stateId = init.getStateId();
        if (commands != null){
            for (Command command: commands){
                try {
                    addedReturn = (AddedReturn) coqtop.execute(new Add(command.getCommand(), stateId, false));
                    stateId = addedReturn.getNextStateId();
                } catch (ErrorException e) {
                    e.printStackTrace();
                }
            }
        }

        return new Pair<>(coqtop, stateId);
    }

    private List<Command> getHistory(Long userId){
        User user = userRepository.findOneByUserId(userId);
        return user.getHistory();
    }
}
