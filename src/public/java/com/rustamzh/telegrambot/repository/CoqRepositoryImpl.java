package com.rustamzh.telegrambot.repository;


import com.rustamzh.coq.Coqtop;
import com.rustamzh.coq.utilites.Pair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

//@Repository
public class CoqRepositoryImpl implements CoqRepository {

    @Autowired
    private ConcurrentHashMap<Long, ImmutableTriple<Coqtop, Integer, Long>> currentUsers;

    @Autowired
    ApplicationContext applicationContext;


    public Pair<Coqtop, Integer> getCoq(Long userId) {
        Triple<Coqtop, Integer, Long> triple = currentUsers.computeIfAbsent(userId, k ->
        {
            Coqtop coqtop = applicationContext.getBean(Coqtop.class);
            return new ImmutableTriple<>(coqtop, 0, System.currentTimeMillis());
        });

        return new Pair<>(triple.getLeft(), triple.getMiddle());
    }

    public boolean updateState(Long userId, int stateId) {
        if (currentUsers.containsKey(userId)) {
            Coqtop coqtop = currentUsers.get(userId).getLeft();
            currentUsers.put(userId, new ImmutableTriple<>(coqtop, stateId, System.currentTimeMillis()));
            return true;
        } else
            return false;
    }

    @Override
    public boolean deleteCoq(Long userId) {
        if (currentUsers.containsKey(userId)){
            currentUsers.remove(userId);
            return true;
        }
        return false;
    }

    @Override
    public boolean isPresent(Long userId) {
        return currentUsers.containsKey(userId);
    }

    public ConcurrentHashMap<Long, ImmutableTriple<Coqtop, Integer, Long>> getCurrentUsers() {
        return currentUsers;
    }

    public void setCurrentUsers(ConcurrentHashMap<Long, ImmutableTriple<Coqtop, Integer, Long>> currentUsers) {
        currentUsers = currentUsers;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
