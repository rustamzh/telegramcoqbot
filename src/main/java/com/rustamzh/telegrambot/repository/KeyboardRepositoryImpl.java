package com.rustamzh.telegrambot.repository;

import com.rustamzh.telegrambot.utilities.StringService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
public class KeyboardRepositoryImpl implements KeyboardRepository {

    @Autowired
    StringService stringService;

    @Override
    public InlineKeyboardMarkup getInlineKeyboardMarkup() {
        InlineKeyboardMarkup replyKeyboard = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboardRowList = new LinkedList<>();
        List<InlineKeyboardButton> keyboardRow1 = new LinkedList<>();
        keyboardRow1.add(new InlineKeyboardButton("apply...").setCallbackData("/addapply"));
        keyboardRow1.add(new InlineKeyboardButton("reflexivity.").setCallbackData("/add reflexivity."));
        keyboardRow1.add(new InlineKeyboardButton("simpl.").setCallbackData("/add simpl."));
        keyboardRow1.add(new InlineKeyboardButton("Qed.").setCallbackData("/add Qed."));
        keyboardRowList.add(keyboardRow1);
        List<InlineKeyboardButton> keyboardRow2 = new LinkedList<>();
        keyboardRow2.add(new InlineKeyboardButton("Undo.").setCallbackData("/add Undo."));
        keyboardRow2.add(new InlineKeyboardButton("Abort.").setCallbackData("/add Abort."));
        keyboardRow2.add(new InlineKeyboardButton("Restart.").setCallbackData("/add Restart."));
        keyboardRow2.add(new InlineKeyboardButton("auto.").setCallbackData("/add auto."));
        keyboardRowList.add(keyboardRow2);
        List<InlineKeyboardButton> keyboardRow3 = new LinkedList<>();
        keyboardRow3.add(new InlineKeyboardButton("intros...").setCallbackData("/addintros"));
        keyboardRow3.add(new InlineKeyboardButton("inversion ...").setCallbackData("/addinversion"));
        keyboardRow3.add(new InlineKeyboardButton("rewrite ...").setCallbackData("/addrewrite"));
        keyboardRow3.add(new InlineKeyboardButton("induction ...").setCallbackData("/addinduction"));
        keyboardRowList.add(keyboardRow3);
        List<InlineKeyboardButton> keyboardRow4 = new LinkedList<>();
        keyboardRow4.add(new InlineKeyboardButton("Other tactic").setCallbackData("/othertactic"));
        keyboardRowList.add(keyboardRow4);
        replyKeyboard.setKeyboard(keyboardRowList);
        return replyKeyboard;
    }

    @Override
    public InlineKeyboardMarkup getLoadPathInlineKeyboardMarkup(Long chatId) {
        InlineKeyboardMarkup replyKeyboard = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboardRowList = new LinkedList<>();
        File folder = new File("cache/documents_"+chatId);
        if (!folder.exists()){
            folder.mkdirs();
        }
        Collection<File> filesCollection = FileUtils.listFiles(folder, new String[]{"v"}, false);
        List<InlineKeyboardButton> buttonList = new LinkedList<>();
        int i = 0;
        for (File file : filesCollection) {
            buttonList.add(new InlineKeyboardButton(StringUtils.removeEnd(file.getName(), ".v")).setCallbackData("/load " + StringUtils.removeEnd(file.getName(), ".v")));
            if (i % 4 == 0) {
                keyboardRowList.add(buttonList);
                buttonList = new LinkedList<>();
            }
        }
        if (i % 4 != 0) {
            keyboardRowList.add(buttonList);
        }
        replyKeyboard.setKeyboard(keyboardRowList);
        return replyKeyboard;
    }

    @Override
    public InlineKeyboardMarkup getInlineKeyboardMarkupFirstButtons() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> buttons = new LinkedList<>();
        List<InlineKeyboardButton> button = new LinkedList<>();
        InlineKeyboardButton keyboardButton1 = new InlineKeyboardButton(stringService.loadButton);
        keyboardButton1.setCallbackData("/require");
        button.add(keyboardButton1);
        InlineKeyboardButton keyboardButton2 = new InlineKeyboardButton(stringService.queryButton);
        keyboardButton2.setCallbackData("/forcequery");
        button.add(keyboardButton2);
        InlineKeyboardButton keyboardButton3 = new InlineKeyboardButton(stringService.startButton);
        keyboardButton3.setCallbackData("/forceadd");
        button.add(keyboardButton3);
        buttons.add(button);
        inlineKeyboardMarkup.setKeyboard(buttons);
        return inlineKeyboardMarkup;
    }
}
