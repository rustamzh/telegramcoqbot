package com.rustamzh.telegrambot.repository;

import com.rustamzh.coq.Coqtop;
import com.rustamzh.coq.utilites.Pair;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

public interface CoqRepository {

    /**
     * Returns pair of state and Coq instance associated with userId
     * If userId is absent will create new pair with state 0
     *
     * @param userId - unique id assiciated with Pair of state and Coq instance
     * @return pair of Coq instance and state
     */

    Pair<Coqtop, Integer> getCoq(Long userId);

    /**
     * Updates the state of the Coq instance associated with userId
     *
     * @param userId - unique id assiciated with Pair of state and Coq instance
     * @param stateId - new state of the Coq instance
     * @return returns true if successfully updated state, otherwise false. Returns false if userId is not in the map
     *
     */
    boolean updateState(Long userId, int stateId);

    /**
     * Updates the state of the Coq instance associated with userId and stores command in database
     *
     * @param userId - unique id assiciated with Pair of state and Coq instance
     * @param stateId - new state of the Coq instance
     * @param command - Vernacular command that will be saved in database
     * @return returns true if successfully updated state, otherwise false. Returns false if userId is not in the map
     *
     */
    default boolean updateState(Long userId, String command, int stateId){
        return updateState(userId,stateId);
    }

    /**
     * Gracefully removes pair of state and Coq instance associated with userId
     * <p>
     * It shutdowns Coq, before deleting pair
     *
     * @param userId - unique id assiciated with Pair of state and Coq instance
     * @return true if entry was deleted, false if it is not present
     */
    boolean deleteCoq(Long userId);

    boolean isPresent(Long userId);

}
