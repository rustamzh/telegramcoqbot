package com.rustamzh.telegrambot;

import com.rustamzh.coq.Coqtop;
import com.rustamzh.coq.commands.*;
import com.rustamzh.coq.feedback.commandsFeedback.*;
import com.rustamzh.coq.utilites.ErrorException;
import com.rustamzh.coq.utilites.Goal;
import com.rustamzh.coq.utilites.Pair;
import com.rustamzh.telegrambot.repository.CoqRepository;
import com.rustamzh.telegrambot.utilities.StringService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.net.URL;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UpdateHandler {

    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private long userId;
    private Coqtop coqtop;
    private int stateId;

    private CoqRepository coqRepository;

    private StringService stringService;

    private void init() {
        if (coqtop == null){
            Pair<Coqtop, Integer> pair = coqRepository.getCoq(userId);
            coqtop = pair.key;
            stateId = pair.value;
        }
    }

    public void downloadFile(String url, String folder, String filename) throws Exception {
        init();
        URL website = new URL(url);
        File file = new File(StringUtils.appendIfMissing(folder, "/") + filename);
        FileUtils.copyURLToFile(website, file);
        if (file.getName().endsWith(".v")) {
            coqtop.compileVintoVO(file);
        }
    }

    public void loadFile(String filename, boolean export) throws ErrorException {
        init();
        String loadCommand = "Add LoadPath \"cache/documents_" + userId + "/\".";
        AddedReturn addedReturn = (AddedReturn) coqtop.execute(new Add(loadCommand , stateId, false));
        coqRepository.updateState(userId, loadCommand, addedReturn.getNextStateId());
        if (!export) {
            String importCommand = "Require Import " + filename + ".";
            addedReturn = (AddedReturn) coqtop.execute(new Add(importCommand, addedReturn.getNextStateId(), false));
            coqRepository.updateState(userId, importCommand, addedReturn.getNextStateId());
        } else {
            String exportCommand = "Require Export " + filename + ".";
            addedReturn = (AddedReturn) coqtop.execute(new Add(exportCommand, addedReturn.getNextStateId(), false));
            coqRepository.updateState(userId, exportCommand, addedReturn.getNextStateId());
        }
    }

    public String queryCoq(String query) {
        init();
        String result = "";
        String error = "";
        try {
            QueryReturn queryReturn = (QueryReturn) coqtop.execute(new Query(query, stateId));
            result = queryReturn.getMessage();

        } catch (ErrorException e) {
            error = e.getErrorList().get(0).getErrMessage();
        }
        return result.equals("") ? error : result;
    }

    public String addCommand(String command) throws Exception{
        init();
        String error = "";
        String goal = "";
        AddedReturn addedReturn;
        try {

            addedReturn = (AddedReturn) coqtop.execute(new Add(command, stateId, true));
            goal = getGoal();

            if (goal.equals("")) {
//                callBackQuery = stringService.callbackNoGoals;
                goal = stringService.noGoals;
            }
        } catch (ErrorException e) {
            error = catchException(e);
            throw new Exception(e.getErrorList().get(0).getErrMessage());
//            callBackQuery = error;
        }
        coqRepository.updateState(userId, command, addedReturn.getNextStateId());
        return goal;
    }

    public String catchException(ErrorException e) {
        init();
        String error = "";
        try {
            //Using bad stateID
            EditAtReturn editAt = (EditAtReturn) coqtop.execute(new EditAt(stateId));
            error = e.getErrorList().get(0).getErrMessage();
        } catch (ErrorException e1) {
            try {
                EditAtReturn editAt = (EditAtReturn) coqtop.execute(new EditAt(e1.getErrorList().get(0).getStateId()));
            } catch (ErrorException e2) {
                LOG.error("Command error, second circle of try-catch, should be unreachable. Please report.");
            }
        }

        return error;
    }

    public void startCommandHandler() throws ErrorException {
        init();
        if (stateId == 0) {
            InitReturn initReturn = (InitReturn) coqtop.execute(new Init());
            coqRepository.updateState(userId, initReturn.getStateId());
        }
    }

    public String getGoal() throws ErrorException {
        init();
        GoalReturn goalReturn = (GoalReturn) coqtop.execute(new Goals());
        return extractGoals(goalReturn);
    }

    public String restartCommandHandler() {
        if (coqRepository.deleteCoq(userId)) {
            return stringService.restarted;
        } else {
            return "Please, first start";
        }
    }

    private String extractGoals(GoalReturn goalReturn) {
        StringBuilder hypotheses = new StringBuilder();
        String goal = "";
        if (goalReturn.getCurrentGoals().isPresent()) {
            int numberOfGoals = goalReturn.getCurrentGoals().get().size();
            Goal current_goal = goalReturn.getCurrentGoals().get().get(0);

            if (current_goal.getHyps().isPresent() && current_goal.getHyps().get().size() != 0) {
                hypotheses = new StringBuilder("===================================================\n");
                for (String hyp : current_goal.getHyps().get()) {
                    hypotheses.append(hyp).append("\n");
                }
            }

            goal = "<pre>" +
                    stringService.headerGoals + numberOfGoals + "\n" +

                    hypotheses +
                    "=========\n" +
                    current_goal.getGoal() +
                    "</pre>";

        }

        return goal;
    }

    public boolean hasStarted() {
        return coqRepository.isPresent(userId);
    }

    public UpdateHandler setUserId(long userId) {

        this.userId = userId;
        return this;
    }

    @Autowired
    public void setCoqRepository(CoqRepository coqRepository) {
        this.coqRepository = coqRepository;
    }

    @Autowired
    public void setStringService(StringService stringService) {
        this.stringService = stringService;
    }
}
