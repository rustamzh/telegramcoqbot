package com.rustamzh.telegrambot.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Service
public class StringService {

    @Value("${app.name}")
    public String appName;

    @Value("${app.version}")
    public String appVersion;

    @Value("${app.credits}")
    public String appCredits;

    @Value("${welcome}")
    public String welcomeString;

    @Value("${help}")
    public String help;

    @Value("${startWarning}")
    public String startWarning;

    @Value("${loadFiles}")
    public String loadFiles;

    @Value("${loadWarning}")
    public String loadWarning;

    @Value("${queryButton}")
    public String queryButton;

    @Value("${loadButton}")
    public String loadButton;

    @Value("${startButton}")
    public String startButton;

    @Value("${importButton}")
    public String importButton;

    @Value("${askForQuery}")
    public String askForQuery;

    @Value("${askForAdd}")
    public String askForAdd;

    @Value("${askForIntros}")
    public String askForIntros;

    @Value("${askForInversion}")
    public String askForInversion;

    @Value("${askForRewrite}")
    public String askForRewrite;

    @Value("${askForInduction}")
    public String askForInduction;

    @Value("${askForApply}")
    public String askForApply;

    @Value("${askForOtherTactic}")
    public String askForOtherTactic;

    @Value("${callbackLoaded}")
    public String callbackLoaded;

    @Value("${callbackNotLoaded}")
    public String callbackNotLoaded;

    @Value("${callbackProcessed}")
    public String callbackProcessed;

    @Value("${callbackError}")
    public String callbackError;

    @Value("${callbackNoGoals}")
    public String callbackNoGoals;

    @Value("${headerGoals}")
    public String headerGoals;

    @Value("${noGoals}")
    public String noGoals;

    @Value("${restarted}")
    public String restarted;



}
