package com.rustamzh.telegrambot;

import com.harium.dotenv.Env;
import com.rustamzh.coq.utilites.ErrorException;
import com.rustamzh.telegrambot.repository.KeyboardRepository;
import com.rustamzh.telegrambot.utilities.StringService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.GetFile;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ForceReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.lang.invoke.MethodHandles;
import java.util.Objects;


/*
 * MIT License
 *
 * Copyright (c) 2018 Rustam Zhumagambetov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@Component
public class CoqBot extends TelegramLongPollingBot {
    static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    private StringService stringService;

    private ApplicationContext applicationContext;

    private KeyboardRepository keyboardRepository;


    public void onUpdateReceived(Update update) {
        SendMessage message = null;
        Long userId;
        String echo = "";
        String callBackQuery = "";
        String error = "";
        UpdateHandler updateHandler;
        try {
            echo = updateExtractor(update);
            userId = getUserId(update);

        } catch (Exception e) {
            return;
        }

        if (update.hasMessage() && update.getMessage().hasDocument()) {
            message = new SendMessage(userId, stringService.loadFiles);
            try {
                getFile(update.getMessage().getDocument().getFileId(), update.getMessage().getDocument().getFileName(), userId);
            } catch (Exception e){
                e.printStackTrace();
                message = new SendMessage(userId, e.getMessage());
            }
        } else if (echo.equals("/version")) {
            message = new SendMessage(userId, stringService.appVersion);
        } else if (echo.equals("/help")) {
            message = new SendMessage(userId, stringService.help);
            message.setParseMode("HTML");

        } else if ((updateHandler = initUpdateHandler(userId)) != null && echo.equals("/start")) {
            echo = stringService.welcomeString;

            try {
                updateHandler.startCommandHandler();
            } catch (ErrorException e) {
                error = updateHandler.catchException(e);
                callBackQuery = error;

            }
            message = new SendMessage(userId, !error.equals("") ? error : echo);
            message.setParseMode("HTML");
            InlineKeyboardMarkup inlineKeyboardMarkup = keyboardRepository.getInlineKeyboardMarkupFirstButtons();
            message.setReplyMarkup(inlineKeyboardMarkup);

        } else if (echo.equals("/require")) {
            message = new SendMessage(userId, stringService.importButton);
            message.setReplyMarkup(keyboardRepository.getLoadPathInlineKeyboardMarkup(userId));

        } else if (echo.equals("/forcequery")) {
            message = new SendMessage(userId, stringService.askForQuery);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.contains("/query")) {

            String query = updateHandler.queryCoq(echo.substring(7));
            message = new SendMessage(userId, query);

        } else if (!Objects.requireNonNull(updateHandler).hasStarted()) {
            message = new SendMessage(userId, stringService.startWarning);

        } else if (echo.equals("/forceadd")) {
            message = new SendMessage(userId, stringService.askForAdd);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/addintros")) {
            message = new SendMessage(userId, stringService.askForIntros);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/addinversion")) {
            message = new SendMessage(userId, stringService.askForInversion);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/addrewrite")) {
            message = new SendMessage(userId, stringService.askForRewrite);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/addinduction")) {
            message = new SendMessage(userId, stringService.askForInduction);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/addapply")) {
            message = new SendMessage(userId, stringService.askForApply);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.equals("/othertactic")) {
            message = new SendMessage(userId, stringService.askForOtherTactic);
            message.setReplyMarkup(new ForceReplyKeyboard());

        } else if (echo.contains("/load")) {

            try {
                updateHandler.loadFile(echo.substring(5), false);
                callBackQuery = stringService.callbackLoaded;

            } catch (ErrorException e) {
                error = updateHandler.catchException(e);
                callBackQuery = error;

            }

        } else if (echo.contains("/add")) {

            try {
                message = new SendMessage(userId, updateHandler.addCommand(echo.substring(5)));
                message.setParseMode("HTML");
                InlineKeyboardMarkup replyKeyboard = keyboardRepository.getInlineKeyboardMarkup();
                message.setReplyMarkup(replyKeyboard);
            } catch (Exception e) {
                message = new SendMessage(userId, e.getMessage());
            }
            if (echo.substring(5).equals("Qed.")) {
                message = new SendMessage(userId, stringService.welcomeString);
                message.setParseMode("HTML");
                InlineKeyboardMarkup inlineKeyboardMarkup = keyboardRepository.getInlineKeyboardMarkupFirstButtons();
                message.setReplyMarkup(inlineKeyboardMarkup);
            }


        } else if (echo.equals("/restart")) {
            echo = updateHandler.restartCommandHandler();
            message = new SendMessage(userId, echo);
        }

        try {
            // If we get any callback, then to avoid spinner, just sending dummy text
            if (update.hasCallbackQuery()) {
                try {
                    execute(new AnswerCallbackQuery().setCallbackQueryId(update.getCallbackQuery().getId()).setText(callBackQuery.equals("") ? stringService.callbackProcessed : callBackQuery));
                } catch (TelegramApiException e1) {
                    e1.printStackTrace();
                }
            }


            if (message != null) {
                Message message1 = execute(message);
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    private UpdateHandler initUpdateHandler(long userId) {
        return applicationContext.getBean(UpdateHandler.class).setUserId(userId);
    }

    private String updateExtractor(Update update) throws Exception {
        if (update.hasMessage()) {
            if (update.getMessage().getReplyToMessage() != null) {
                return catchButtons(update.getMessage().getReplyToMessage().getText(), update.getMessage().getText());
            }
            return update.getMessage().getText();

        } else if (update.hasCallbackQuery()) {
            return update.getCallbackQuery().getData();
        }
        throw new Exception("Not supported yet");
    }


    private String catchButtons(String replyToMessage, String echo) {
        if (replyToMessage.equals(stringService.askForQuery)) {

            echo = "/query " + StringUtils.appendIfMissing(echo, ".");

        } else if (replyToMessage.equals(stringService.askForAdd)) {

            echo = "/add " + StringUtils.appendIfMissing(echo, ".");

        } else if (replyToMessage.equals(stringService.askForIntros)) {

            echo = "/add intros " + StringUtils.appendIfMissing(echo, ".");

        } else if (replyToMessage.equals(stringService.askForInversion)) {

            echo = "/add inversion " + StringUtils.appendIfMissing(echo, ".");

        } else if (replyToMessage.equals(stringService.askForRewrite)) {

            echo = "/add rewrite " + StringUtils.appendIfMissing(echo, ".");

        } else if (replyToMessage.equals(stringService.askForInduction)) {

            echo = "/add induction " + StringUtils.appendIfMissing(echo, ".");
        } else if (replyToMessage.equals(stringService.askForApply)) {

            echo = "/add apply " + StringUtils.appendIfMissing(echo, ".");
        } else if (replyToMessage.equals(stringService.askForOtherTactic)) {

            echo = "/add " + StringUtils.appendIfMissing(echo, ".");
        }
        return echo;
    }


    private void getFile(String fileId, String filename, Long userId) throws Exception{
        if (filename.endsWith(".v") || filename.endsWith(".vo")) {
            org.telegram.telegrambots.api.objects.File file = execute(new GetFile().setFileId(fileId));
            initUpdateHandler(userId).downloadFile(file.getFileUrl(getBotToken()), "cache/documents_" + userId + "/", filename);
        } else {
            throw new Exception(stringService.loadWarning);
        }
    }


    public String getBotUsername() {

        return Env.get("TelegramBotName");
    }

    public String getBotToken() {

        return Env.get("TelegramBotToken");
    }

    private long getUserId(Update update) throws Exception {
        if (update.hasMessage()) {
            return update.getMessage().getChatId();

        } else if (update.hasCallbackQuery()) {
            return update.getCallbackQuery().getMessage().getChatId();
        }
        throw new Exception("Not supported yet.");
    }

    @Autowired
    public void setStringService(StringService stringService) {
        this.stringService = stringService;
    }

    @Autowired
    public void setKeyboardRepository(KeyboardRepository keyboardRepository) {
        this.keyboardRepository = keyboardRepository;
    }

    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
