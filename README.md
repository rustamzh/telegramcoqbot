# Telegram bot

An IDE for Coq interactive theorem prover.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Currently bot is working only with Coq 8.6 and linux distro with bash.


### Installing

1. Clone repository
2. Build jar file `./gradlew privateBootJar` or `./gradlew privateBootJar`
3. Prepare
    - Telegram bot Name (from [BotFather](https://core.telegram.org/bots#creating-a-new-bot))
    - Telegram bot token (from [BotFather](https://core.telegram.org/bots#creating-a-new-bot))
    - Telegram username (your own username in telegram) for private flavour
    - export them as `TelegramBotName` `TelegramBotToken` `TelegramUsername`
4. Run app `java -jar ./build/libs/coq-telegrambot-${version}-${flavour}.jar` **OR**
5. Use docker
   -  Install docker
   -  Step 2.
   -  `./gradlew create${flavour}Dockerfile`
   -  `docker build -t telegrambot:latest ./build/libs/`
   -  Put your `TelegramBotName` `TelegramBotToken` `TelegramUsername` 
   in .env file and run 
   ```
   docker run -d -t --init --name=telegrambot --restart=always --env-file .env telegrambot:latest
   ```
6. Start using the bot with `/start` command

## Running the tests

No tests yet.

## Built With

* [Telegram Bot Java Library](https://github.com/rubenlagus/TelegramBots/) - The telegram bot framework
* [Gradle](https://gradle.org/) - Dependency Management
* [Spring](https://spring.io/) - Dependency injection framework
* [Dropwizard metrics](metrics.dropwizard.io) - Metrics for public repo
* [InfluxDB Repoter](https://github.com/davidB/metrics-influxdb) - Library for reporting metrics to InfluxDB
* [Dotenv](https://github.com/Harium/dotenv) - Utility for handling .env file

